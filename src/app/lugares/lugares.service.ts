import { BehaviorSubject, of} from 'rxjs';
import { HttpClient } from "@angular/common/http";
import { Injectable} from '@angular/core';
import { Lugar } from './lugar.model';
import { LoginService } from "./../login/login.service";
import { take, map, tap, delay, switchMap } from "rxjs/operators";
import { LugarUbicacion } from "./location.model";


interface LugarData{
  descripcion: string;
  disponibleDesde: string;
  disponibleHasta: string;
  id: number;
  imageUrl: string;
  precio: number;
  titulo: string;
  usuarioId: number;
  ubicacion: LugarUbicacion
}

@Injectable({
  providedIn: 'root'
})
export class LugaresService {
    
  private _lugares = new BehaviorSubject<Lugar[]>([ 
  /*new Lugar(1, 'Quinta Gonzalez', 'Quinta con excelente ubicacción', 'https://s2.qwant.com/thumbr/700x0/4/b/5569d71556984435a20faadf90faccc4ce7b942872bb5dce74fc53e70a3a56/Enclave+Mountain+Estates.jpg?u=http%3A%2F%2F3.bp.blogspot.com%2F-L46lTVCs1ik%2FUoUmRanCvcI%2FAAAAAAAAEwo%2FtfoTWqzk3jw%2Fw1200-h630-p-k-no-nu%2FEnclave%2BMountain%2BEstates.jpg&q=0&b=1&p=0&a=1',1200, new Date('2020-10-01'), new Date('2021-01-01'), 1),
  new Lugar(2, 'Depto. Las Torres', 'Apartamento con excelente ubicación', 'https://s2.qwant.com/thumbr/0x380/f/3/d04fd0aefc7f8b0a2cc459f3ffc691ffd7ba123520b4dd6733eed32d6ef7f6/1200px-Luzon_Apartment_Building.JPG?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fthumb%2Fc%2Fcf%2FLuzon_Apartment_Building.JPG%2F1200px-Luzon_Apartment_Building.JPG&q=0&b=1&p=0&a=1', 2400, new Date('2020-10-01'), new Date('2021-01-01'), 1),
  new Lugar(3, 'Cumbre Elite', 'Apartamento con excelente ubicación', 'https://s2.qwant.com/thumbr/0x380/d/6/0ee11be73be0597ee2b38383f7f754370c96cb883613c82c450f43a7b07928/San-Jose.jpg?u=https%3A%2F%2Fwww.myfirstapartment.com%2Fwp-content%2Fuploads%2F2016%2F03%2FSan-Jose.jpg&q=0&b=1&p=0&a=1', 1800, new Date('2020-10-01'), new Date('2021-01-01'), 1),
  new Lugar(11, 'Quinta Gonzalez', 'Quinta con excelente ubicacción', 'https://s2.qwant.com/thumbr/700x0/4/b/5569d71556984435a20faadf90faccc4ce7b942872bb5dce74fc53e70a3a56/Enclave+Mountain+Estates.jpg?u=http%3A%2F%2F3.bp.blogspot.com%2F-L46lTVCs1ik%2FUoUmRanCvcI%2FAAAAAAAAEwo%2FtfoTWqzk3jw%2Fw1200-h630-p-k-no-nu%2FEnclave%2BMountain%2BEstates.jpg&q=0&b=1&p=0&a=1',1200, new Date('2020-10-01'), new Date('2021-01-01'), 1),
  new Lugar(12, 'Depto. Las Torres', 'Apartamento con excelente ubicación', 'https://s2.qwant.com/thumbr/0x380/f/3/d04fd0aefc7f8b0a2cc459f3ffc691ffd7ba123520b4dd6733eed32d6ef7f6/1200px-Luzon_Apartment_Building.JPG?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fthumb%2Fc%2Fcf%2FLuzon_Apartment_Building.JPG%2F1200px-Luzon_Apartment_Building.JPG&q=0&b=1&p=0&a=1', 2400, new Date('2020-10-01'), new Date('2021-01-01'), 1),
  new Lugar(13, 'Cumbre Elite', 'Apartamento con excelente ubicación', 'https://s2.qwant.com/thumbr/0x380/d/6/0ee11be73be0597ee2b38383f7f754370c96cb883613c82c450f43a7b07928/San-Jose.jpg?u=https%3A%2F%2Fwww.myfirstapartment.com%2Fwp-content%2Fuploads%2F2016%2F03%2FSan-Jose.jpg&q=0&b=1&p=0&a=1', 1800, new Date('2020-10-01'), new Date('2021-01-01'), 1),
  new Lugar(21, 'Quinta Gonzalez', 'Quinta con excelente ubicacción', 'https://s2.qwant.com/thumbr/700x0/4/b/5569d71556984435a20faadf90faccc4ce7b942872bb5dce74fc53e70a3a56/Enclave+Mountain+Estates.jpg?u=http%3A%2F%2F3.bp.blogspot.com%2F-L46lTVCs1ik%2FUoUmRanCvcI%2FAAAAAAAAEwo%2FtfoTWqzk3jw%2Fw1200-h630-p-k-no-nu%2FEnclave%2BMountain%2BEstates.jpg&q=0&b=1&p=0&a=1',1200, new Date('2020-10-01'), new Date('2021-01-01'), 1),
  new Lugar(22, 'Depto. Las Torres', 'Apartamento con excelente ubicación', 'https://s2.qwant.com/thumbr/0x380/f/3/d04fd0aefc7f8b0a2cc459f3ffc691ffd7ba123520b4dd6733eed32d6ef7f6/1200px-Luzon_Apartment_Building.JPG?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fthumb%2Fc%2Fcf%2FLuzon_Apartment_Building.JPG%2F1200px-Luzon_Apartment_Building.JPG&q=0&b=1&p=0&a=1', 2400, new Date('2020-10-01'), new Date('2021-01-01'), 1),
  new Lugar(23, 'Cumbre Elite', 'Apartamento con excelente ubicación', 'https://s2.qwant.com/thumbr/0x380/d/6/0ee11be73be0597ee2b38383f7f754370c96cb883613c82c450f43a7b07928/San-Jose.jpg?u=https%3A%2F%2Fwww.myfirstapartment.com%2Fwp-content%2Fuploads%2F2016%2F03%2FSan-Jose.jpg&q=0&b=1&p=0&a=1', 1800, new Date('2020-10-01'), new Date('2021-01-01'), 1),
  new Lugar(31, 'Quinta Gonzalez', 'Quinta con excelente ubicacción', 'https://s2.qwant.com/thumbr/700x0/4/b/5569d71556984435a20faadf90faccc4ce7b942872bb5dce74fc53e70a3a56/Enclave+Mountain+Estates.jpg?u=http%3A%2F%2F3.bp.blogspot.com%2F-L46lTVCs1ik%2FUoUmRanCvcI%2FAAAAAAAAEwo%2FtfoTWqzk3jw%2Fw1200-h630-p-k-no-nu%2FEnclave%2BMountain%2BEstates.jpg&q=0&b=1&p=0&a=1',1200, new Date('2020-10-01'), new Date('2021-01-01'), 1),
  new Lugar(32, 'Depto. Las Torres', 'Apartamento con excelente ubicación', 'https://s2.qwant.com/thumbr/0x380/f/3/d04fd0aefc7f8b0a2cc459f3ffc691ffd7ba123520b4dd6733eed32d6ef7f6/1200px-Luzon_Apartment_Building.JPG?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fthumb%2Fc%2Fcf%2FLuzon_Apartment_Building.JPG%2F1200px-Luzon_Apartment_Building.JPG&q=0&b=1&p=0&a=1', 2400, new Date('2020-10-01'), new Date('2021-01-01'), 1),
  new Lugar(33, 'Cumbre Elite', 'Apartamento con excelente ubicación', 'https://s2.qwant.com/thumbr/0x380/d/6/0ee11be73be0597ee2b38383f7f754370c96cb883613c82c450f43a7b07928/San-Jose.jpg?u=https%3A%2F%2Fwww.myfirstapartment.com%2Fwp-content%2Fuploads%2F2016%2F03%2FSan-Jose.jpg&q=0&b=1&p=0&a=1', 1800, new Date('2020-10-01'), new Date('2021-01-01'), 1),
  new Lugar(41, 'Quinta Gonzalez', 'Quinta con excelente ubicacción', 'https://s2.qwant.com/thumbr/700x0/4/b/5569d71556984435a20faadf90faccc4ce7b942872bb5dce74fc53e70a3a56/Enclave+Mountain+Estates.jpg?u=http%3A%2F%2F3.bp.blogspot.com%2F-L46lTVCs1ik%2FUoUmRanCvcI%2FAAAAAAAAEwo%2FtfoTWqzk3jw%2Fw1200-h630-p-k-no-nu%2FEnclave%2BMountain%2BEstates.jpg&q=0&b=1&p=0&a=1',1200, new Date('2020-10-01'), new Date('2021-01-01'), 1),
  new Lugar(42, 'Depto. Las Torres', 'Apartamento con excelente ubicación', 'https://s2.qwant.com/thumbr/0x380/f/3/d04fd0aefc7f8b0a2cc459f3ffc691ffd7ba123520b4dd6733eed32d6ef7f6/1200px-Luzon_Apartment_Building.JPG?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fthumb%2Fc%2Fcf%2FLuzon_Apartment_Building.JPG%2F1200px-Luzon_Apartment_Building.JPG&q=0&b=1&p=0&a=1', 2400, new Date('2020-10-01'), new Date('2021-01-01'), 1),
  new Lugar(43, 'Cumbre Elite', 'Apartamento con excelente ubicación', 'https://s2.qwant.com/thumbr/0x380/d/6/0ee11be73be0597ee2b38383f7f754370c96cb883613c82c450f43a7b07928/San-Jose.jpg?u=https%3A%2F%2Fwww.myfirstapartment.com%2Fwp-content%2Fuploads%2F2016%2F03%2FSan-Jose.jpg&q=0&b=1&p=0&a=1', 1800, new Date('2020-10-01'), new Date('2021-01-01'), 1)
  */
]);
   

  get lugares(){
    return this._lugares.asObservable();
  }

  fetchLugares(){
    return this.http.get<{[key: string]: LugarData}>('https://base-labam.firebaseio.com/ofertas-lugares.json')
    .pipe(map(dta => {
      const lugares = [];
      for(const key in dta){
        if(dta.hasOwnProperty(key)){
          lugares.push(
            //dta
            new Lugar(dta[key].id, dta[key].titulo, dta[key].descripcion, dta[key].imageUrl, dta[key].precio, new Date(dta[key].disponibleDesde), new Date(dta[key].disponibleHasta), dta[key].usuarioId, key, dta[key].ubicacion)
          )
        }
      }
      return lugares;
    }),
    tap(lugares => {
      this._lugares.next(lugares)
    }))
  }

  constructor(private loginService: LoginService, private http: HttpClient) { }

  getLugar(firebaseId: string){
    const url = `https://base-labam.firebaseio.com/ofertas-lugares/${firebaseId}.json`;
    console.log(url);
    return this.http.get<LugarData>(`https://base-labam.firebaseio.com/ofertas-lugares/${firebaseId}.json`)
    .pipe(map(dta => {
      return new Lugar(dta.id, dta.titulo, dta.descripcion, dta.imageUrl, dta.precio, new Date(dta.disponibleDesde), new Date(dta.disponibleHasta), dta.usuarioId, firebaseId, dta.ubicacion);
    }));
  }

  addLugar(titulo: string, descripcion: string, precio: number, disponibleDesde: Date, disponibleHasta: Date, ubicacion: LugarUbicacion){
    const newLugar = new Lugar(Math.random(), titulo, descripcion, 'https://img10.naventcdn.com/avisos/18/00/53/55/97/00/720x532/144271181.jpg', precio, disponibleDesde, disponibleHasta, this.loginService.usuarioId, '', ubicacion);
    this.http.post<any>('https://base-labam.firebaseio.com/ofertas-lugares.json', {...newLugar, firebaseId: null}).subscribe(data => {
      console.log(data);
      return this._lugares.pipe(take(1)).subscribe(lugares => {
        this._lugares.next(lugares.concat(newLugar));
      });
    });
  }

  updateLugar(lugarId: string, titulo: string, descripcion: string){
    let nuevosLugares: Lugar[];
    return this.lugares.pipe(take(1), 
    switchMap(lugares => {
      if(lugares || lugares.length <= 0){
        return this.fetchLugares();
      }
      else{
        return of(lugares);
      }
    }),
    switchMap(lugares => {
      const index = lugares.findIndex(lu => lu.firebaseId === lugarId);
      nuevosLugares = [...lugares];
      const old = nuevosLugares[index];
      nuevosLugares[index] = new Lugar(old.id, titulo, descripcion, old.imageUrl, old.precio, old.disponibleDesde, old.disponibleHasta, old.usuarioId, '', old.ubicacion);
      return this.http.put(`https://base-labam.firebaseio.com/ofertas-lugares/${lugarId}.json`, {...nuevosLugares[index]});
    }), tap(() => {
      this._lugares.next(nuevosLugares);
    })
    );
  }  
}
