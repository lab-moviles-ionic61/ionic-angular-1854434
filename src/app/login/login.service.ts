import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private _usuarioLoggeado = true;
  private _usuarioId = 1;

  get usuarioLoggeado(){
    return this._usuarioLoggeado;
  }

  get usuarioId(){
    return this._usuarioId;
  }
  constructor() { }

  Login(){
    this._usuarioLoggeado = true;
  }

  Logout(){
    this._usuarioLoggeado = false;
  }
}
