import { Component, OnInit } from '@angular/core';
import { LoginService } from './login.service';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { NgForm } from "@angular/forms";

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  isLoading: boolean = false;
  isLoginMode: boolean = true;

  constructor(private loginService: LoginService, private router: Router,
    private LoadingCtrl: LoadingController) { }

  ngOnInit() {
    this.isLoading = true;
  }

  onLogin(){
    this.isLoading = true;
    this.loginService.Login();
    this.LoadingCtrl.create({keyboardClose: true, duration: 1000, message: 'Cargando ...'})
    .then(loadingEl => {
      loadingEl.present();
      setTimeout(() => {
        this.isLoading = false;
        this.router.navigateByUrl('/lugares/tabs/busqueda')
      },1000);
    });
  }

  onSubmit(form: NgForm){
    if(!form.valid){
      return false;
    }
    const email = form.value.email;
    const password = form.value.password;
    if(this.isLoginMode){
      //LLAMADA A WS DE LOGIN
    }
    else{
      //LLAMADA A WS DE REGISTRO
    }
  }

  onSwitchAuthMode(){
    this.isLoginMode = !this.isLoginMode;
  }
}
